// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on.
// Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on.
// There could be typing mistakes in the string so if the number is invalid, return 0.

function convertStringToNumber(inputString) {
  let string = "";
  if (inputString[0] === "$") {
    //to check if first character is "$"
    string = inputString.substring(1); //to get the string after "$"
  } else if (inputString[0] === "-") {
    //to check if first character is "-"
    string = inputString.substring(2); //to get the string after "-$"
  }

  let stringToNumber = Number(string); //to convert string to number

  if (!isNaN(stringToNumber)) {
    //to check if the converted number is valid or not
    return stringToNumber;
  } else {
    return 0;
  }
}

module.exports = convertStringToNumber;
