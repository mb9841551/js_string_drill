// Given an IP address - "111.139.161.143".
// Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values.
// [111, 139, 161, 143].

function convertIpToNumeric(inputString) {
  const emptyArray = [];
  const numericValues = inputString.split("."); //to split the inputString with separator(.)
  if (numericValues.length === 4) {
    const resultArray = numericValues.map((index) => {
      //using map to convert all string values in numericValues array into numbers
      return parseInt(index);
    });
    return resultArray;
  } else {
    return emptyArray;
  }
}

module.exports = convertIpToNumeric; //to export the above function to testfile
