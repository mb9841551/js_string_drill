// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function displayFullName(inputArray) {
  const namesArrayTitleCase = inputArray.map((word) => {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  });

  const fullName = namesArrayTitleCase.join(" ");

  return fullName;
}

function putFullNameInArray(inputObject) {
  const names = Object.values(inputObject);
  return displayFullName(names);
}

module.exports = putFullNameInArray;
