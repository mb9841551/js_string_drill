// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

function convertToString(inputArray) {
  if (inputArray.legth != 0) {
    const result = inputArray.join(" "); //use join method to join the array and return as string
    return result;
  } else {
    return "";
  }
}

module.exports = convertToString;
