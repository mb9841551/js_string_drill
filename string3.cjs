// Given a string in the format of "20/1/2021", print the month in which the date is present in.

function printMonth(inputString) {
  const dateMonthYear = inputString.split("/"); //to split the string using separator(/)
  let month = Number(dateMonthYear[1]); //get month using indexing

  if (month <= 12 && month >= 1) {
    return month;
  } else {
    return "Not valid";
  }
}

module.exports = printMonth; //to export the printMonth function to test file
